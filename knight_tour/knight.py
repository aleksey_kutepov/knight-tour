# coding: utf8
import argparse
import random

from knight_tour import knight_simple
from knight_tour import utils




# Note: all matrices need to be transposed to suite regular board look.
weight_matrix = []
board_matrix = []


def get_available_moves(x_current, y_current, include_visited=False):
    board_range = range(8)
    return [(x, y) for x, y in [
        (x_current - 2, y_current - 1),
        (x_current - 2, y_current + 1),
        (x_current - 1, y_current - 2),
        (x_current - 1, y_current + 2),
        (x_current + 1, y_current - 2),
        (x_current + 1, y_current + 2),
        (x_current + 2, y_current - 1),
        (x_current + 2, y_current + 1)
    ] if x in board_range and y in board_range
        and (include_visited or weight_matrix[x][y] != 0)]


def warnsdorff_chose_rnd(available_moves):
    """Warnsdorff method with random choice from equal squares.

    Finds best move based on weight matrix. I.e. square with minimum
    available moves from. Weight matrix represents current situation.

    """
    best_moves = []
    min_weight = 9
    for x, y in available_moves:
        weight = weight_matrix[x][y]
        if weight < min_weight:
            min_weight = weight
            best_moves = [(x, y)]
        elif weight == min_weight:
            best_moves.append((x, y))
    best_move = random.choice(best_moves or [None])
    return best_move


def heuristic_found_path(starting_cell):
    """Solves the Knight's tour problem on regular 8x8 chess board.

    Uses Warnsdorff's rule.

    Args:
        starting_cell: Tuple. Starting square on chess board (x, y).

    """
    global weight_matrix
    global board_matrix
    while True:
        weight_matrix = utils.reset_weights()
        board_matrix = utils.reset_board()
        path = chain_missing_cells(starting_cell)
        update_board_by_new_path(path)
        if not utils.has_uncovered_cells(weight_matrix):
            return path


def update_board_by_new_path(new_path):
    """Redraws board by the new path"""
    global board_matrix
    new_board = utils.reset_board()
    for n, (x, y) in enumerate(new_path):
        new_board[x][y] = n + 1
    board_matrix = new_board


def chain_missing_cells(starting_cell):
    """Main chaining function"""
    path = [starting_cell]
    current_cell = starting_cell
    while True:
        available_moves = get_available_moves(*current_cell)
        if not available_moves:
            break
        utils.update_weight_matrix_on_move(
            current_cell[0], current_cell[1], available_moves, weight_matrix)
        best_move = warnsdorff_chose_rnd(available_moves)
        assert best_move is not None
        path.append(best_move)
        current_cell = best_move
    return path


def parse_arguments():
    def validate_cell_args(notation):
        error = utils.validate_cell(notation)
        if error:
            raise argparse.ArgumentTypeError(error)
        return notation
    arg_parser = argparse.ArgumentParser(
        description="Knight's tour problem solution")
    arg_parser.add_argument('-s', '--square',
                            default='d4',
                            type=validate_cell_args,
                            help='Starting square in the format "d4".'
                                 ' Default is "d4".')
    arg_parser.add_argument('-f', '--fast',
                            action='store_true',
                            help='Use fast method on static path. By default '
                                 '"slow" heuristic method is used.')
    return arg_parser.parse_args()


def main():
    args = parse_arguments()
    if args.fast:
        path = knight_simple.super_fast_precalculated_path(args.square)
        update_board_by_new_path((utils.get_xy_from_notation(n) for n in path))
    else:
        cell = utils.get_xy_from_notation(args.square)
        path = [utils.get_notation_from_xy(*xy)
                for xy in heuristic_found_path(cell)]
    utils.print_board(board_matrix, 'Board moves')
    print 'Moves: {0}.'.format(
        ', '.join(utils.get_moves_notation_from_path(path)))
    errors = utils.validate_path(path)
    if errors:
        print "Constructed path is incorrect. Errors {0}".format(errors)


if __name__ == '__main__':
    main()

