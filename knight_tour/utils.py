def reset_board():
    return [
        ['--'] * 8,
        ['--'] * 8,
        ['--'] * 8,
        ['--'] * 8,
        ['--'] * 8,
        ['--'] * 8,
        ['--'] * 8,
        ['--'] * 8,
    ]


def reset_weights():
    return [
        # 1  2  3  4  5  6  7  8
        [2, 3, 4, 4, 4, 4, 3, 2],  # a
        [3, 4, 6, 6, 6, 6, 4, 3],  # b
        [4, 6, 8, 8, 8, 8, 6, 4],  # c
        [4, 6, 8, 8, 8, 8, 6, 4],  # d
        [4, 6, 8, 8, 8, 8, 6, 4],  # e
        [4, 6, 8, 8, 8, 8, 6, 4],  # f
        [3, 4, 6, 6, 6, 6, 4, 3],  # g
        [2, 3, 4, 4, 4, 4, 3, 2],  # h
        # 1  2  3  4  5  6  7  8
    ]


def get_xy_from_notation(chess_square):
    return ord(chess_square[0]) - 97, int(chess_square[1]) - 1


def get_notation_from_xy(x, y):
    return chr(x+97) + str(y+1)


def get_move_numbers_from_board(board_matrix, cells):
    return [board_matrix[x][y] for x, y in cells if
            not isinstance(board_matrix[x][y], basestring)]


def shift_end_path_to_position(path, move_number):
    return path[move_number:] + path[:move_number]


def print_board(matrix, title='\n'):
    """Print transposed matrix"""
    print title
    print '   a  b  c  d  e  f  g  h'
    print '   ----------------------'
    transposed = list(reversed(zip(*matrix)))
    l_it = iter(xrange(len(transposed), 0, -1))
    for line in transposed:
        ln = next(l_it)
        print '{0}|'.format(ln),

        for cell in line:
            print '{0:>2}'.format(str(cell)),
        print ' |{0}'.format(ln)
    print '   ----------------------'
    print '   a  b  c  d  e  f  g  h'


def has_uncovered_cells(weight_matrix):
    return any(sum(line) != 0 for line in weight_matrix)


def update_weight_matrix_on_move(x, y, cells_to_remove_link, weight_matrix):
    weight_matrix[x][y] = 0
    for x_, y_ in cells_to_remove_link:
        if weight_matrix[x_][y_] != 0:
            weight_matrix[x_][y_] -= 1


def get_moves_notation_from_path(path):
    for pair in zip(path[:-1], path[1:]):
        yield '{0} - {1}'.format(*pair)


def validate_cell(notation):
    if not isinstance(notation, basestring):
        return 'Cell {0} is not string'.format(notation)
    if len(notation) != 2:
        return 'Cell {0} has invalid notation'.format(notation)
    x_board_range = range(97, 104 + 1)
    y_board_range = range(1, 9)

    x = ord(notation[0])
    try:
        y = int(notation[1])
    except (TypeError, ValueError):
        return 'Cell {0} has invalid notation'.format(notation)
    else:
        if x not in x_board_range or y not in y_board_range:
            return 'Cell {0} out of board'.format(notation)


def validate_path(path):
    errors = []
    length = len(path)
    if length != 64:
        errors.append('Path length {0} is not 64'.format(length))
    for pos, cell in enumerate(path):
        error = validate_cell(cell)
        if error:
            errors.append(error + ' at position {0}'.format(pos))
    if len(errors):
        return errors

    # check whether knight moves correct
    pair_of_moves = zip(path[:-1], path[1:])
    for move_no, (left, right) in enumerate(pair_of_moves):
        delta_x = abs(ord(left[0]) - ord(right[0]))
        delta_y = abs(ord(left[1]) - ord(right[1]))
        if not ((delta_x == 1 and delta_y == 2)
                or (delta_x == 2 and delta_y == 1)):
            errors.append('Invalid move #{0}: {1} - {2}'.
                          format(move_no + 1, left, right))
    return errors
