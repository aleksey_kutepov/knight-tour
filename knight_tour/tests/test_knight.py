from knight_tour import knight, knight_simple, utils


def test_knight():
    for x in range(8):
        for y in range(8):
            path = [utils.get_notation_from_xy(*xy)
                    for xy in knight.heuristic_found_path((x, y))]
            errors = utils.validate_path(path)
            if errors:
                print "Errors {0}".format(errors)


def test_knight_simple():
    for x in range(8):
        for y in range(8):
            path = knight_simple.super_fast_precalculated_path(
                utils.get_notation_from_xy(x, y))
            errors = utils.validate_path(path)
            if errors:
                print "Errors {0}".format(errors)

if __name__ == '__main__':
    test_knight()
    test_knight_simple()
