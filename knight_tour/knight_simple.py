"""This method is considered as hack of Knight Tour problem.

We get known cyclic knight's path and then returning slices of the path from
the given starting point."""

from knight_tour import utils


# This one was discovered by a Chess Machine "Turk"
_chess_machines_path = [
    'd4', 'f5', 'd6', 'e8', 'c7', 'a8', 'b6', 'a4', 'b2', 'd1', 'f2', 'h1',
    'g3', 'h5', 'g7', 'e6', 'f8', 'd7', 'b8', 'a6', 'b4', 'a2', 'c1', 'e2',
    'g1', 'h3', 'f4', 'd3', 'c5', 'e4', 'c3', 'd5', 'e3',
    'c4', 'e5', 'c6', 'd8', 'b7', 'a5', 'b3', 'a1', 'c2', 'e1', 'g2', 'h4',
    'g6', 'h8', 'f7', 'h6', 'g4', 'h2', 'f1', 'd2', 'b1', 'a3', 'b5', 'a7',
    'c8', 'e7', 'g8', 'f6', 'h7', 'g5', 'f3'
]


def validate_or_exit(path):
    e = utils.validate_path(path)
    if len(e):
        print e
        exit(1)


def super_fast_precalculated_path(starting_cell):
    """Constructs path from the previously calculated one.

    This function can be used for speed reasons. Since the path is cycle,
    we can take it's starting point at any graph node.

    Args:
        starting_cell: Starting square on chess board in the 'a1' notation.

    """
    pos = _chess_machines_path.index(starting_cell)
    return _chess_machines_path[pos:64] + _chess_machines_path[0:pos]


def main():

    # Just smoke test before
    validate_or_exit(_chess_machines_path)
    new_path = super_fast_precalculated_path('d1')
    validate_or_exit(new_path)
    print new_path


if __name__ == '__main__':
    main()
